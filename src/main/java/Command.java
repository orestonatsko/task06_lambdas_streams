@FunctionalInterface
public interface Command {
    void execute(String s);
}
