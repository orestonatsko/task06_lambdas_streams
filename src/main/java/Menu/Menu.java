package Menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Menu {
    private Map<String, Printable> menu;
    private List<String> menuItems;
    private InputHandler inputHandler;

    public Menu() {
        inputHandler = new InputHandler();
        menuItems = new ArrayList<>();
        menuItems.add("Enter three values");
//        menuItems.add("2 - choose item");
//        menuItems.add("3 - choose item");
//        menuItems.add("4 - choose item");
//        menuItems.add("Q - choose item");
        menu = new HashMap<>();
        menu.put("1", this::executeItem1);
        menu.put("2", () -> System.out.println("You choose item 2"));
        menu.put("3", this::executeItem3);
        menu.put("4", this::executeItem4);
    }

    private void executeItem1() {
        System.out.println("You choose item 1");
    }

    private void executeItem2() {
        System.out.println("You choose item 2");
    }

    private void executeItem3() {
        System.out.println("You choose item 3");
    }

    private void executeItem4() {
        System.out.println("You choose item 4");
    }

    public void show() {
        menuItems.forEach(System.out::println);
        int choice = inputHandler.nextInt();
        menu.get(choice).print();
    }
}
