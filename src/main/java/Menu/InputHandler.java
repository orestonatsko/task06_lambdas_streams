package Menu;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputHandler {

    int nextInt(int menuItems) {
        int input = 0;
        do {
            System.out.println("Your choice:");
            Scanner scanner = new Scanner(System.in);
            try {
                input = scanner.nextInt();
                if (input < 1 || input > menuItems) {
                    error();
                }
            } catch (InputMismatchException e) {
                error();
            }
        } while (input < 1 || input > menuItems);
        return input;
    }

    public int nextInt() {
        int input = 0;
        while (input == 0) {
            System.out.println("Enter value:");
            Scanner scanner = new Scanner(System.in);
            try {
                input = scanner.nextInt();

            } catch (InputMismatchException e) {
                error();
            }
        }
        return input;
    }

    private void error() {
        System.out.println("INCORRECT INPUT!");
    }

    public String nextLine() {
        String input = null;
        while (input == null) {
            System.out.println("Enter value:");
            Scanner scanner = new Scanner(System.in);
            try {
                input = scanner.nextLine();

            } catch (InputMismatchException e) {
                error();
            }
        }
        return input;
    }
}
