import Menu.InputHandler;
import Menu.Menu;
import Selector.Selector;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.show();
        System.out.println("Enter three values:");
        InputHandler handler = new InputHandler();
        int[] values = new int[3];
        for (int i = 0; i < 3; i++) {
            values[i] = handler.nextInt();
        }
        Selector maxSelector = (a, b, c) -> Math.max(Math.max(a, b), c);
        Selector avSelector = (a, b, c) -> (a + b + c) / 3;
        int maxVal = App.select(values, maxSelector);
        int averageVal = App.select(values, avSelector);
        System.out.println(maxVal);
        System.out.println(averageVal);

        String command = handler.nextLine();

        executeCommand(command, new Command() {
            @Override
            public void execute(String s) {
                System.out.println(command);
            }
        });
        executeCommand(command, System.out::print);
        executeCommand(command, (s) -> System.out.println(s));
        MyCommand myCommand = new MyCommand();
        executeCommand(command, myCommand);

    }

    private static int[] generateIntArray(){
        return   ThreadLocalRandom.current().ints().limit(10).toArray();
    }
    private static List<Integer> generateList(){
        Random r = new Random();
        return  Stream.of(1, 2, 3, 4, 5).map(v->{
          int rand =   r.nextInt(10 - 1);
          return rand + v;
        }).collect(Collectors.toList());
    }

    private static int select(int[] arr, Selector selector) {
        return selector.select(arr[0], arr[1], arr[2]);
    }

    private static void executeCommand(String s, Command cmd) {
        cmd.execute(s);
    }
    private static  int getSum(List<Integer> list){
        return list.stream().mapToInt(Integer::intValue).sum();
    }
    private static  int getSumFromReduce(List<Integer> list){
        return list.stream().reduce((a,b)-> a +b).get();
    }
}
