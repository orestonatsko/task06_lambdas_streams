package Selector;

public interface Selector {

    int select(int v1, int v2, int v3);
}
